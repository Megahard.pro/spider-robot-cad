# Spider robot CAD

CAD files for the spider robot as seen on www.megahard.pro

These are CATIAv5 files. To get started dump all files in a directory and load the _Entire spider.CATProduct_ file. 

License: Attribution-ShareAlike 2.5 Generic (CC BY-SA 2.5) 
https://creativecommons.org/licenses/by-sa/2.5/